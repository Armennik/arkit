

import UIKit
import ARKit

extension ViewController: VirtualObjectSelectionViewControllerDelegate {
    
    
 
    
    func changeColor(_ color: UIColor){
        if virtualObjectInteraction.selectedCube == nil || (virtualObjectInteraction.selectedCube?.parent?.name != "Chair.scn" && virtualObjectInteraction.selectedCube?.parent?.name != "table.scn"){
            return
        }
        virtualObjectInteraction.isSelectedObject(selected: false)
        virtualObjectInteraction.selectedCube!.parent!.childNodes.map {
            $0.childNodes.map{
                if $0.geometry != nil {
                    for i in  ($0.geometry?.materials)! {
                        i.diffuse.contents = color
                    }
                }
            }
            if $0.geometry != nil {
                for i in  ($0.geometry?.materials)! {
                    i.diffuse.contents = color
                }
            }
        }
        virtualObjectInteraction.selectedCube!.geometry?.materials[0].diffuse.contents = color
    }
    
    // MARK: - VirtualObjectSelectionViewControllerDelegate
    
    func virtualObjectSelectionViewController(_: VirtualObjectSelectionViewController, didSelectObject object: String) {
        self.selectedVirtualOBjectName = object
       
    }
    
    func virtualObjectSelectionViewController(_: VirtualObjectSelectionViewController?, didDeselectObject object: String) {
          self.selectedVirtualOBjectName = nil
    }


  
}
