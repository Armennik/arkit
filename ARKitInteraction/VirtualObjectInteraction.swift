

import UIKit
import ARKit


class VirtualObjectInteraction: NSObject, UIGestureRecognizerDelegate {
    
    let translateAssumingInfinitePlane = true
    let chairCategory: Int = 1 << 0
    let lampCategory: Int  = 1 << 1
    let floorCategory: Int = 1 << 2
    let tableCategory: Int = 1 << 3
    var floorPlane: SCNNode?
    let sceneView: VirtualObjectARView
    var panGesture:UIPanGestureRecognizer?
    var lockGesture = false
    var counter = 0;
    var distination : SCNVector3?
    var selectedCube : SCNNode?
 
    var parrentViewController: ViewController?
    private var currentTrackingPosition: CGPoint?

    init(sceneView: VirtualObjectARView, controller: ViewController) {
        self.sceneView = sceneView
        super.init()
        parrentViewController = controller
        panGesture = UIPanGestureRecognizer.init(target: self, action: #selector(didPan(_:)))
        panGesture!.delegate = self
        panGesture?.minimumNumberOfTouches = 1
        let rotationGesture = UIRotationGestureRecognizer(target: self, action: #selector(didRotate(_:)))
        rotationGesture.delegate = self
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(didTap(_:)))
        tapGesture.numberOfTapsRequired = 2
        let singltapGesture = UITapGestureRecognizer(target: self, action: #selector(longPress(_:)))
        singltapGesture.numberOfTapsRequired = 1
        let scaleGesture = UIPinchGestureRecognizer(target: self, action: #selector(scalePiece(_:)))
        singltapGesture.require(toFail: tapGesture)
        
 
        
        sceneView.addGestureRecognizer(panGesture!)
        sceneView.addGestureRecognizer(rotationGesture)
        sceneView.addGestureRecognizer(tapGesture)
        sceneView.addGestureRecognizer(scaleGesture)
        sceneView.addGestureRecognizer(singltapGesture)
    }
    
    // MARK: - Gesture Actions
    
    var prevusPonit: SCNVector3?
    var PCoordx: Float = 0.0
    var PCoordz: Float = 0.0
    
    @objc
    func didPan(_ gesture: UIPanGestureRecognizer) {
        if lockGesture {
             selectedCube?.parent?.physicsBody?.isAffectedByGravity = true
            return
        }
       
        switch gesture.state {
        case .began:

            let holdPoint = gesture.location(in: self.sceneView)
            let result = self.sceneView.hitTest(holdPoint, types: .estimatedHorizontalPlane)
            if (result.count == 0) {
                return;
            }
            let  hitResult = result.first
            let transform = hitResult?.worldTransform
            let dist = SCNVector3.init((transform?.columns.3.x)!, (transform?.columns.3.y)!, (transform?.columns.3.z)!)
            prevusPonit = dist
            PCoordx = (result.first?.worldTransform.columns.3.x)!
            PCoordz = (result.first?.worldTransform.columns.3.z)!

        case .changed:
            if selectedCube == nil  {
                return
            }
            let holdPoint = gesture.location(in: self.sceneView)
            let result = self.sceneView.hitTest(holdPoint, types: .existingPlane)
            let hitTestResult = self.sceneView.hitTest(holdPoint, options: nil)
            if (result.count == 0) {
                return;
            }
            
            if hitTestResult.count > 0  && hitTestResult.first?.node.name != "floor" && hitTestResult.first?.node.name != "plane" && hitTestResult.first?.node.parent!.name != selectedCube?.parent?.name{
                self.distination = SCNVector3.init((hitTestResult.first?.worldCoordinates.x)!, (hitTestResult.first?.worldCoordinates.y)! + 0.001, (hitTestResult.first?.worldCoordinates.z)!)
            }else{
            let  hitResult = result.first
            let transform = hitResult?.worldTransform
            let dist = SCNVector3.init((transform?.columns.3.x)!, (transform?.columns.3.y)!, (transform?.columns.3.z)!)
            self.distination = SCNVector3.init(dist.x, dist.y, dist.z)
            }
            gesture.setTranslation(CGPoint.zero, in: sceneView)
            break
            
        case .ended:
            selectedCube?.parent?.physicsBody?.isAffectedByGravity = true
            if (selectedCube != nil) {
                self.isSelectedObject(selected: false)
                selectedCube = nil
            }
            prevPos = nil
            break
        default:
            currentTrackingPosition = nil
        }
    }
    
    var prevPos : SCNVector3?
    
    func updatePos(){
        if distination == nil || selectedCube == nil {
            return
        }
        if prevPos == nil {
            prevPos = distination
        }
        if  prevPos!.x != distination!.x  || prevPos!.y != distination!.y  || prevPos!.z != distination!.z {
            SCNTransaction.begin()
            selectedCube?.parent?.position = distination!
            SCNTransaction.commit()
            prevPos = distination
        }
    }

    
    @objc func scalePiece(_ gestureRecognizer : UIPinchGestureRecognizer) {
        if gestureRecognizer.state == .began {
            selectedCube?.parent?.physicsBody?.isAffectedByGravity = false
            lockGesture = true

        }
        if gestureRecognizer.state == .changed {
            if selectedCube == nil {
                return
            }
            SCNTransaction.begin()
            let pinchScaleX = Float(gestureRecognizer.scale) * selectedCube!.scale.x
            let pinchScaleY = Float(gestureRecognizer.scale) * selectedCube!.scale.y
            let pinchScaleZ = Float(gestureRecognizer.scale) * selectedCube!.scale.z
            selectedCube!.scale = SCNVector3(pinchScaleX,pinchScaleY,pinchScaleZ)
            selectedCube!.parent!.physicsBody?.physicsShape = SCNPhysicsShape.init(node: selectedCube!.parent!, options:nil)
            SCNTransaction.commit()
            gestureRecognizer.scale = 1
            
            
        }
        if gestureRecognizer.state == .ended{
            selectedCube?.parent?.physicsBody?.isAffectedByGravity = true
            if selectedCube == nil {
                return
            }
            let pinchScaleX = Float(gestureRecognizer.scale) * selectedCube!.scale.x
            let pinchScaleY = Float(gestureRecognizer.scale) * selectedCube!.scale.y
            let pinchScaleZ = Float(gestureRecognizer.scale) * selectedCube!.scale.z
            selectedCube!.scale = SCNVector3(pinchScaleX,pinchScaleY,pinchScaleZ)
            selectedCube!.parent!.physicsBody?.physicsShape = SCNPhysicsShape.init(node: selectedCube!.parent!, options:nil)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                self.lockGesture = false
            }
            if (selectedCube != nil) {
                self.isSelectedObject(selected: false)
                selectedCube = nil
            }
        }
        
    }
  
  
 

    @objc
    func didRotate(_ gesture: UIRotationGestureRecognizer) {
        lockGesture = true
        if selectedCube == nil {
            return
        }
        guard gesture.state == .changed else { return }
        let newValue = (selectedCube?.eulerAngles.y)! - Float(gesture.rotation)
        var normalized = newValue.truncatingRemainder(dividingBy: 2 * .pi)
        normalized = (normalized + 2 * .pi).truncatingRemainder(dividingBy: 2 * .pi)
        if normalized > .pi {
            normalized -= 2 * .pi
        }
        selectedCube?.eulerAngles.y = normalized
        selectedCube!.parent!.physicsBody?.physicsShape = SCNPhysicsShape.init(node: selectedCube!.parent!, options:nil)
        
        gesture.rotation = 0
       
        if gesture.state == .ended{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                self.lockGesture = false
            }
        selectedCube?.parent?.physicsBody?.isAffectedByGravity = true
        if (selectedCube != nil) {
            self.isSelectedObject(selected: false)
            selectedCube = nil
        }
        }
    }
    
    func isSelectedObject(selected: Bool){
        if selected {
        for  material in (selectedCube?.geometry!.materials)! {
        SCNTransaction.begin()
        SCNTransaction.animationDuration = 0.5
        
        material.emission.contents = UIColor.red
        
        SCNTransaction.commit()
    }
    }      else {
                for  material in (selectedCube?.geometry!.materials)! {
                    material.emission.contents = UIColor.clear
                }
    }
        }

    
    
    @objc
    func longPress(_ gesture: UILongPressGestureRecognizer) {
        if selectedCube == nil {
            if let object = objectNodeInteracting(with: gesture, in: sceneView) {
                if object.name == "plane" || object.name == "floor"{
                    return
                }
                selectedCube = object
                self.isSelectedObject(selected: true)
            }
            return
        }
        let holdPoint = gesture.location(in: self.sceneView)
        let hitTestResult = self.sceneView.hitTest(holdPoint, options: nil)
        if hitTestResult.count < 0 {
            return
        }
        
        distination = hitTestResult.first?.worldCoordinates
//        if (selectedCube != nil) {
//            self.isSelectedObject(selected: false)
//            selectedCube = nil
//        }
    }
    
    
    @objc
    func didTap(_ gesture: UITapGestureRecognizer) {
        
        
        guard let objectName = self.parrentViewController?.selectedVirtualOBjectName else {return}
        
        let touchLocation = gesture.location(in: sceneView)
            let result = self.sceneView.hitTest(touchLocation, types: .existingPlane)
            if (result.count == 0) {
                return;
            }
            let  hitResult = result.first
            let transform = hitResult?.worldTransform
            let dist = SCNVector3.init((transform?.columns.3.x)!, (transform?.columns.3.y)!, (transform?.columns.3.z)!)
            let scene = SCNScene.init(named: objectName)
            guard let node  = scene?.rootNode else {return}
            var scale : CGFloat = 0.03
            if node.boundingBox.max.x - node.boundingBox.min.x  > 12 {
                scale = 0.002
            }
            let pinchScaleX: CGFloat = scale * CGFloat((node.scale.x))
            let pinchScaleY: CGFloat = scale * CGFloat((node.scale.y))
            let pinchScaleZ: CGFloat = scale * CGFloat((node.scale.z))
            node.scale = SCNVector3.init(pinchScaleX, pinchScaleY, pinchScaleZ)
            node.physicsBody = SCNPhysicsBody.init(type: .dynamic, shape: nil)
            node.position = dist
            node.name = objectName
            node.physicsBody?.categoryBitMask = (objectName == "Chair.scn") ? chairCategory : tableCategory
            node.physicsBody?.contactTestBitMask = (objectName == "Chair.scn") ? chairCategory : tableCategory
            node.physicsBody?.mass = 1
            sceneView.scene.rootNode.addChildNode(node)
           if (selectedCube != nil) {
                self.isSelectedObject(selected: false)
                selectedCube = nil
            }
        
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }


  
    
    private func objectNodeInteracting(with gesture: UIGestureRecognizer, in view: ARSCNView) -> SCNNode? {
        for index in 0..<gesture.numberOfTouches {
            let touchLocation = gesture.location(ofTouch: index, in: view)
            
            if let object = sceneView.virtualNodeObject(at: touchLocation) {
                return object
            }
            
        }
        
        return sceneView.virtualNodeObject(at: gesture.center(in: view))
    }
    

    /// - Tag: DragVirtualObject
    
}

extension UIGestureRecognizer {
    func center(in view: UIView) -> CGPoint {
        let first = CGRect(origin: location(ofTouch: 0, in: view), size: .zero)

        let touchBounds = (1..<numberOfTouches).reduce(first) { touchBounds, index in
            return touchBounds.union(CGRect(origin: location(ofTouch: index, in: view), size: .zero))
        }

        return CGPoint(x: touchBounds.midX, y: touchBounds.midY)
    }
}
