
import ARKit
import SceneKit
import UIKit

class ViewController: UIViewController ,SCNPhysicsContactDelegate{
    
    @IBOutlet var sceneView: VirtualObjectARView!
    @IBOutlet weak var addObjectButton: UIButton!
    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    var colorPicker:ColorPickerView?
    var planes = [UUID: Plane]()
    var selectedVirtualOBjectName: String?
    
    lazy var statusViewController: StatusViewController = {
        return childViewControllers.lazy.compactMap({ $0 as? StatusViewController }).first!
    }()
    
    var objectsViewController: VirtualObjectSelectionViewController?
    lazy var virtualObjectInteraction = VirtualObjectInteraction(sceneView: sceneView, controller: self)
    var isRestartAvailable = true
    let updateQueue = DispatchQueue(label: "testQueue")
    
    var screenCenter: CGPoint {
        let bounds = sceneView.bounds
        return CGPoint(x: bounds.midX, y: bounds.midY)
    }
    
    var session: ARSession {
        return sceneView.session
    }
    
    let chairCategory: Int = 1 << 0
    let lampCategory:  Int = 1 << 1
    let floorCategory: Int = 1 << 2
    let tableCategory: Int = 1 << 3
    
    
    @IBAction func colorChangeAction(_ sender: UIButton) {
        let colorPickerframe = CGRect.init(x: 10, y: 30, width: 300, height: 30)
        colorPicker = ColorPickerView(frame: colorPickerframe)
        colorPicker!.transform = CGAffineTransform(rotationAngle: CGFloat(-M_PI_2))
        colorPicker!.didChangeColor = { color in
          DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()  + 3){
                self.colorPicker?.removeFromSuperview()
                self.virtualObjectInteraction.selectedCube = nil
            }
            
            self.changeModelColorTo(color: color!)
        }
        self.view.addSubview(colorPicker!)
        colorPicker!.center = CGPoint.init(x: 25, y: self.view.frame.size.height - 230)
        UIApplication.shared.keyWindow?.bringSubview(toFront: colorPicker!)
        sender.tag = 1
        
    }
    @IBAction func snapshotAction(_ sender: UIButton) {
        let image = self.sceneView.snapshot()
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
        let imageView  = UIImageView.init(frame: CGRect.init(x: 5, y: self.view.frame.size.height - 130, width: 60, height: 120))
        imageView.image = image
        imageView.contentMode = .scaleAspectFit
        imageView.layer.borderWidth = 2
        imageView.layer.borderColor = UIColor.white.cgColor
        self.view.addSubview(imageView)
        UIApplication.shared.keyWindow?.bringSubview(toFront: imageView)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()  + 3){
            imageView.removeFromSuperview()
        }
        
    }
    
    func changeModelColorTo(color: UIColor){
        self.changeColor(color)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        sceneView.delegate = self
        sceneView.session.delegate = self
       // sceneView.debugOptions = .showPhysicsShapes
        
        self.sceneView.scene.physicsWorld.contactDelegate = self;
        setupCamera()
        sceneView.setupDirectionalLighting(queue: updateQueue)
        statusViewController.restartExperienceHandler = { [unowned self] in
            self.restartExperience()
        }
        
        
        let bottomPlane = SCNBox.init(width: 100, height: 0.1, length: 100, chamferRadius: 0)
        bottomPlane.firstMaterial!.diffuse.contents = UIColor.red
        let bottomNode = SCNNode.init(geometry: bottomPlane)
        bottomNode.position = SCNVector3.init(0, -10, 0)
        bottomNode.name = "floor"
        bottomNode.physicsBody = SCNPhysicsBody.init(type: .kinematic, shape: nil)
        bottomNode.geometry!.materials[0].diffuse.contents = UIColor.clear
        bottomNode.physicsBody!.categoryBitMask = Int(floorCategory)
        bottomNode.physicsBody!.contactTestBitMask = Int(chairCategory) | Int(tableCategory)
        bottomNode.physicsBody!.collisionBitMask = Int(chairCategory) | Int(tableCategory)
        sceneView.scene.rootNode.addChildNode(bottomNode)
    }
    
  
    func physicsWorld(_ world: SCNPhysicsWorld, didBegin contact: SCNPhysicsContact){
        
        if contact.nodeA.physicsBody == nil || contact.nodeB.physicsBody == nil {
            return
        }
        let contactMask = contact.nodeA.physicsBody!.categoryBitMask | contact.nodeB.physicsBody!.categoryBitMask
        
        
        
        if (contactMask == (chairCategory | floorCategory) || contactMask == (tableCategory | floorCategory) ) {
            if (contact.nodeA.name == "table.scn") {
                contact.nodeA.removeFromParentNode()
            }
            if (contact.nodeB.name == "table.scn") {
                contact.nodeB.removeFromParentNode()
               
            }
            if (contact.nodeA.name == "Chair.scn") {
                contact.nodeA.removeFromParentNode()
            }
            if (contact.nodeB.name == "Chair.scn") {
                contact.nodeB.removeFromParentNode()
            }
            
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        UIApplication.shared.isIdleTimerDisabled = true
        resetTracking()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        session.pause()
    }


    func setupCamera() {
        guard let camera = sceneView.pointOfView?.camera else {
            fatalError("Expected a valid `pointOfView` from the scene.")
        }

        camera.wantsHDR = true
        camera.exposureOffset = -1
        camera.minimumExposure = -1
        camera.maximumExposure = 3
    }

    
    func resetTracking() {
        virtualObjectInteraction.selectedCube = nil
        let configuration = ARWorldTrackingConfiguration()
        configuration.planeDetection = [.horizontal]
        if #available(iOS 12.0, *) {
            configuration.environmentTexturing = .automatic
        }
        session.run(configuration, options: [.resetTracking, .removeExistingAnchors])
        statusViewController.scheduleMessage("FIND A SURFACE TO PLACE AN OBJECT", inSeconds: 7.5, messageType: .planeEstimation)
    }


  
    
    
    func displayErrorMessage(title: String, message: String) {
        blurView.isHidden = false
        // Present an alert informing about the error that has occurred.
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let restartAction = UIAlertAction(title: "Restart Session", style: .default) { _ in
            alertController.dismiss(animated: true, completion: nil)
            self.blurView.isHidden = true
            self.resetTracking()
        }
        alertController.addAction(restartAction)
        present(alertController, animated: true, completion: nil)
    }

}
